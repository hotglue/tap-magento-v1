"""Stream type classes for tap-magentov1."""
from singer_sdk import typing as th

from tap_magento_v1.client import Magentov1Stream
from typing import Optional, Iterable
from singer_sdk.helpers.jsonpath import extract_jsonpath
import requests
class ProductsStream(Magentov1Stream):
    """Define Products stream."""

    name = "products"
    path = "/products"
    primary_keys = ["entity_id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("entity_id", th.StringType),
        th.Property("attribute_set_id", th.StringType),
        th.Property("type_id", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("manufacturer", th.StringType),
        th.Property("size_format", th.StringType),
        th.Property("tax_class_id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("visibility", th.StringType),
        th.Property("product_type", th.StringType),
        th.Property("color", th.StringType),
        th.Property("is_imported", th.StringType),
        th.Property("remove_compare_link", th.StringType),
        th.Property("ebizmarts_mark_visited", th.StringType),
        th.Property("cjm_hideshipdate", th.StringType),
        th.Property("eraser_shape", th.StringType),
        th.Property("style_option", th.StringType),
        th.Property("colour_option", th.StringType),
        th.Property("paper_type", th.StringType),
        th.Property("guest_hide_price", th.StringType),
        th.Property("pack_size", th.StringType),
        th.Property("price", th.StringType),
        th.Property("weight", th.StringType),
        th.Property("special_price", th.StringType),
        th.Property("msrp", th.StringType),
        th.Property("description", th.StringType),
        th.Property("meta_keyword", th.StringType),
        th.Property("short_description", th.StringType),
        th.Property("searchindex_weight", th.StringType),
        th.Property("custom_layout_update", th.StringType),
        th.Property("mobile_description", th.StringType),
        th.Property("cjm_stockmessage", th.StringType),
        th.Property("useful_for", th.StringType),
        th.Property("news_to_date", th.StringType),
        th.Property("news_from_date", th.StringType),
        th.Property("special_from_date", th.DateTimeType),
        th.Property("special_to_date", th.DateTimeType),
        th.Property("custom_design_from", th.DateTimeType),
        th.Property("custom_design_to", th.DateTimeType),
        th.Property("cjm_expecdate", th.DateTimeType),
        th.Property("cjm_preorderdate", th.DateTimeType),
        th.Property("name", th.StringType),
        th.Property("meta_title", th.StringType),
        th.Property("meta_description", th.StringType),
        th.Property("options_container", th.StringType),
        th.Property("size_inches", th.StringType),
        th.Property("msrp_enabled", th.StringType),
        th.Property("size_metric", th.StringType),
        th.Property("msrp_display_actual_price_type", th.StringType),
        th.Property("qty_per_pack", th.StringType),
        th.Property("url_key", th.StringType),
        th.Property("country_of_manufacture", th.StringType),
        th.Property("product_barcode", th.StringType),
        th.Property("isbn", th.StringType),
        th.Property("custom_design", th.StringType),
        th.Property("page_layout", th.StringType),
        th.Property("cjm_stocktext", th.StringType),
        th.Property("cjm_ships_in", th.StringType),
        th.Property("cjm_preordertext", th.StringType),
        th.Property("gift_message_available", th.StringType),
        th.Property("xero_item_code", th.StringType),
        th.Property("hs_code", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_id": record["entity_id"],
        }


class OrdersStream(Magentov1Stream):
    """Define Orders stream."""

    name = "orders"
    path = "/orders"
    primary_keys = ["entity_id"]
    replication_key = None
    
    @property
    def page_size(self):
        return self.config.get("orders_page_size", 100)

    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("coupon_code", th.StringType),
        th.Property("shipping_description", th.StringType),
        th.Property("customer_id", th.StringType),
        th.Property("base_discount_amount", th.StringType),
        th.Property("base_grand_total", th.StringType),
        th.Property("base_shipping_amount", th.StringType),
        th.Property("base_shipping_tax_amount", th.StringType),
        th.Property("base_subtotal", th.StringType),
        th.Property("base_tax_amount", th.StringType),
        th.Property("base_total_paid", th.StringType),
        th.Property("base_total_refunded", th.StringType),
        th.Property("discount_amount", th.StringType),
        th.Property("grand_total", th.StringType),
        th.Property("shipping_amount", th.StringType),
        th.Property("shipping_tax_amount", th.StringType),
        th.Property("store_to_order_rate", th.StringType),
        th.Property("subtotal", th.StringType),
        th.Property("tax_amount", th.StringType),
        th.Property("total_paid", th.StringType),
        th.Property("total_refunded", th.StringType),
        th.Property("base_shipping_discount_amount", th.StringType),
        th.Property("base_subtotal_incl_tax", th.StringType),
        th.Property("base_total_due", th.StringType),
        th.Property("shipping_discount_amount", th.StringType),
        th.Property("subtotal_incl_tax", th.StringType),
        th.Property("total_due", th.StringType),
        th.Property("increment_id", th.StringType),
        th.Property("base_currency_code", th.StringType),
        th.Property("discount_description", th.StringType),
        th.Property("remote_ip", th.StringType),
        th.Property("store_currency_code", th.StringType),
        th.Property("store_name", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property(
            "addresses",
            th.ArrayType(
                th.ObjectType(
                    th.Property("region", th.StringType),
                    th.Property("postcode", th.StringType),
                    th.Property("lastname", th.StringType),
                    th.Property("street", th.StringType),
                    th.Property("city", th.StringType),
                    th.Property("email", th.StringType),
                    th.Property("telephone", th.StringType),
                    th.Property("country_id", th.StringType),
                    th.Property("firstname", th.StringType),
                    th.Property("address_type", th.StringType),
                    th.Property("prefix", th.StringType),
                    th.Property("middlename", th.StringType),
                    th.Property("suffix", th.StringType),
                    th.Property("company", th.StringType),
                )
            ),
        ),
        th.Property(
            "order_items",
            th.ArrayType(
                th.ObjectType(
                    th.Property("item_id", th.StringType),
                    th.Property("parent_item_id", th.StringType),
                    th.Property("sku", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("qty_canceled", th.StringType),
                    th.Property("qty_invoiced", th.StringType),
                    th.Property("qty_ordered", th.StringType),
                    th.Property("qty_refunded", th.StringType),
                    th.Property("qty_shipped", th.StringType),
                    th.Property("price", th.StringType),
                    th.Property("base_price", th.StringType),
                    th.Property("original_price", th.StringType),
                    th.Property("base_original_price", th.StringType),
                    th.Property("tax_percent", th.StringType),
                    th.Property("tax_amount", th.StringType),
                    th.Property("base_tax_amount", th.StringType),
                    th.Property("discount_amount", th.StringType),
                    th.Property("base_discount_amount", th.StringType),
                    th.Property("row_total", th.StringType),
                    th.Property("base_row_total", th.StringType),
                    th.Property("price_incl_tax", th.StringType),
                    th.Property("base_price_incl_tax", th.StringType),
                    th.Property("row_total_incl_tax", th.StringType),
                    th.Property("base_row_total_incl_tax", th.StringType),
                )
            ),
        ),
        th.Property(
            "order_comments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("is_customer_notified", th.StringType),
                    th.Property("is_visible_on_front", th.StringType),
                    th.Property("comment", th.StringType),
                    th.Property("status", th.StringType),
                    th.Property("created_at", th.DateTimeType),
                )
            ),
        ),
    ).to_dict()

class ProductCategoriesStream(Magentov1Stream):
    """Define Orders stream."""

    name = "productcategories"
    path = "/products/{product_id}/categories"
    primary_keys = ["product_id","category_id"]
    replication_key = None
    parent_stream_type = ProductsStream
    schema = th.PropertiesList(
        th.Property("product_id", th.StringType),
        th.Property("category_id", th.StringType)
    ).to_dict()
    def parse_response(self, response: requests.Response) -> Iterable[dict]:

        for records in extract_jsonpath(self.records_jsonpath, input=response.json()):
            product_id = response.url.split(f"{self.url_base}/products/")
            product_id = product_id[1].split("/categories")
            product_id = product_id[0]
            for row in records.values():
                return_row = {"product_id":product_id,"category_id":row}
                yield return_row
class StockItemsStream(Magentov1Stream):
    """Define Orders stream."""

    name = "stockitems"
    path = "/stockitems"
    primary_keys = ["item_id"]
    records_jsonpath = "$[*]"
    replication_key = None
    schema = th.PropertiesList(
        
        th.Property("item_id", th.StringType),
        th.Property("product_id", th.StringType),
        th.Property("stock_id", th.StringType),
        th.Property("qty", th.StringType),
        th.Property("min_qty", th.StringType),
        th.Property("use_config_min_qty", th.StringType),
        th.Property("is_qty_decimal", th.StringType),
        th.Property("backorders", th.StringType),
        th.Property("use_config_backorders", th.StringType),
        th.Property("min_sale_qty", th.StringType),
        th.Property("use_config_min_sale_qty", th.StringType),
        th.Property("max_sale_qty", th.StringType),
        th.Property("use_config_max_sale_qty", th.StringType),
        th.Property("is_in_stock", th.StringType),
        th.Property("low_stock_date", th.StringType),
        th.Property("notify_stock_qty", th.StringType),
        th.Property("use_config_notify_stock_qty", th.StringType),
        th.Property("manage_stock", th.StringType),
        th.Property("use_config_manage_stock", th.StringType),
        th.Property("stock_status_changed_auto", th.StringType),
        th.Property("use_config_qty_increments", th.StringType),
        th.Property("qty_increments", th.StringType),
        th.Property("use_config_enable_qty_inc", th.StringType),
        th.Property("enable_qty_increments", th.StringType),
        th.Property("is_decimal_divided", th.StringType),
    ).to_dict()
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())