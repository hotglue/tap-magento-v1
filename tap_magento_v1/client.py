"""REST client handling, including Magentov1Stream base class."""

import copy
from typing import Any, Callable, Dict, Generator, Iterable, Optional, cast

import backoff
import requests
from requests_oauthlib import OAuth1
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
import datetime

class Magentov1Stream(RESTStream):
    """Magentov1 stream class."""

    page_size = 100

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["store_url"].strip("/") + "/api/rest"

    records_jsonpath = "$.[*]"

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if self.name=="stockitems" or self.name=="productcategories":
            if len(response.json()) < self.page_size:
                return None    
        
        if len(response.json().keys()) < self.page_size:
            return None

        return (previous_token or 1) + 1
    
    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        start_date = self.config.get("start_date")
        if self.page_size:
            params["limit"] = self.page_size
        if next_page_token:
            params["page"] = next_page_token 

        if start_date and self.name=="orders":
            if isinstance(start_date,datetime.datetime):
                start_date = start_date.strftime('%Y-%m-%dT%H:%M:%S.%f')    
            params['filter[1][attribute]'] = "created_at"
            params['filter[1][from]'] = start_date
        return params

    @property
    def generate_oauth(self):
        config = self.config
        oauth = OAuth1(
            config.get("consumer_key"),
            client_secret=config.get("consumer_secret"),
            resource_owner_key=config.get("oauth_token", config.get("access_token")),
            resource_owner_secret=config.get("oauth_token_secret", config.get("access_token_secret")),
            signature_type="auth_header",
        )
        return oauth

    def parse_response(self, response: requests.Response) -> Iterable[dict]:

        for record in extract_jsonpath(self.records_jsonpath, input=response.json()):
            for row in record.values():
                yield row

    def backoff_wait_generator(self) -> Callable[..., Generator[int, Any, None]]:
        return backoff.expo(factor=5)

    def backoff_max_tries(self) -> int:
        return 10

    def _request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.Response:

        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
            params.update(authenticator.auth_params or {})

        prepared_request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    json=request_data,
                    auth=self.generate_oauth,
                ),
            ),
        )

        response = self.requests_session.send(prepared_request, timeout=self.timeout)
        if self._LOG_REQUEST_METRICS:
            extra_tags = {}
            if self._LOG_REQUEST_METRIC_URLS:
                extra_tags["url"] = prepared_request.path_url
            self._write_request_duration_log(
                endpoint=self.path,
                response=response,
                context=context,
                extra_tags=extra_tags,
            )
        self.validate_response(response)
        return response


    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Request records from REST endpoint(s), returning response records. """
        next_page_token: Any = None
        finished = False
        decorated_request = self.request_decorator(self._request)

        while not finished:
            resp = decorated_request(context, next_page_token=next_page_token)
            yield from self.parse_response(resp)
            previous_token = copy.deepcopy(next_page_token)
            next_page_token = self.get_next_page_token(
                response=resp, previous_token=previous_token
            )
            if next_page_token and next_page_token == previous_token:
                raise RuntimeError(
                    f"Loop detected in pagination. "
                    f"Pagination token {next_page_token} is identical to prior token."
                )
            # Cycle until get_next_page_token() no longer returns a value
            finished = not next_page_token
