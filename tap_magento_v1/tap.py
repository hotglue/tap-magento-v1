"""Magentov1 tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_magento_v1.streams import OrdersStream, ProductsStream, ProductCategoriesStream,StockItemsStream

STREAM_TYPES = [ProductsStream, OrdersStream, ProductCategoriesStream,StockItemsStream]


class TapMagentov1(Tap):
    """Magentov1 tap class."""

    name = "tap-magento-v1"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "consumer_key",
            th.StringType,
            required=True,
        ),
        th.Property(
            "consumer_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "store_url",
            th.StringType,
            required=True,
        ),
        th.Property(
            "access_token",
            th.StringType,
            required=False,
        ),
        th.Property(
            "access_token_secret",
            th.StringType,
            required=False,
        ),
        th.Property(
            "oauth_token",
            th.StringType,
            required=False,
        ),
        th.Property(
            "oauth_token_secret",
            th.StringType,
            required=False,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapMagentov1.cli()
